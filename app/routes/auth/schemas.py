from pydantic import BaseModel


class Login(BaseModel):
    mail: str
    passwd: str


class CreateUser(BaseModel):
    mail: str
    passwd: str
    type_: int
    first_name: str
    last_name: str
    title: str
