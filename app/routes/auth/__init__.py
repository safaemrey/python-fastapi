from sqlalchemy.orm import mapper
from datetime import datetime
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from werkzeug.security import generate_password_hash, check_password_hash

from app.db.db_funcs import get_db
from .schemas import Login, CreateUser
from app.db.models import *
from app.db.crud.user_basic_crud import userBasicCrud
from app.db.models.ma.user_ma import UserBasicMaWithDetails, UserBasicMaWithOUTDetails
from app.exceptions import PasswordNotCorrectError, UserNotFoundError


auth = APIRouter()


@auth.post('/createUser')
def login(createUser: CreateUser, db: Session = Depends(get_db)):
    """
    yeni kullanıcı oluşturma urli ve işlemleri
    """
    user = UserBasic()  # basic verileri ekleniyo
    user.mail = createUser.mail
    user.pw_hash = generate_password_hash(createUser.passwd)
    user.type_ = createUser.type_
    user.first_name = createUser.first_name
    user.last_name = createUser.last_name
    db.add(user)
    db.commit()
    # basicler db ye kayıt ediliyor. details verilerde bunun id si gerekli olduğu için commit ediliyor.

    # details veriler için instance oluşuyor ve alanlar doluyor.
    userDetails = UserDetails()
    userDetails.user_id = user.id
    userDetails.title = createUser.title
    db.add(userDetails)
    db.commit()
    # details veriler de kaydediliyor. commit işlemi yapınca otomatik refresh ediliyo user nesnesi.

    # pw kısmını ayırarak bir ma nesnesi oluşuyor.
    userBasicMaWithDetails = UserBasicMaWithDetails(exclude=("pw_hash",))
    # bu nesne kullanılarak oluşan kullanıcımız dump ediliyor. direkt olarak dict döndüğü için return etmede sakınca yok
    userWith = userBasicMaWithDetails.dump(user)

    userBasicMaWithOUTDetails = UserBasicMaWithOUTDetails(
        exclude=("pw_hash", "details"))
    userWithout = userBasicMaWithOUTDetails.dump(user)

    return {"result": userWith,
            "result1": userWithout,
            }


@auth.post('/login')
def login(login: Login, db: Session = Depends(get_db)):
    """
    giriş metodu. mail ve şifre bilgileri eşleşirse token return edecek.
    daha sonraki request'ler bu token ile yapılacak.
    """
    code: int
    message: str
    data = None
    try:
        result = userBasicCrud.login(db=db, login=login)
    except UserNotFoundError as e:
        code = e.code
        message = e.message
    except PasswordNotCorrectError as e:
        code = e.code
        message = e.message
    else:
        code = 200
        message = 'Başarılı'
        data = result

    return {"code": code,
            "message": message,
            "data": data
            }


@auth.get('/test')
def test(db: Session = Depends(get_db)):
    """
    test
    """
    data = userBasicCrud.get_multi(db)
    return {"data": data}

# ############################ TEST ALANI ##################################
# import jwt

# encoded = jwt.encode({'public_id': 'c58e1f35-6ee4-4ba0-8a64-63546823188d', "expire_date_time": "123213212312"},
#                      key="55cc5c007adc16f1c072294a0806e9a303e720fb6024b3a7423873aa8cc46d33", algorithm='HS256')
# print("bu encoded", encoded)
# decoded = jwt.decode(
#     encoded, key="55cc5c007adc16f1c072294a0806e9a303e720fb6024b3a7423873aa8cc46d33", algorithms='HS256')
# print("bu decoded", decoded)
# print()
# date = datetime.now()
# print("bu datetime normal", date)
# timest = datetime.timestamp(date)
# print("bu timestamp", timest)
# print("bu micro secli fromtimestamps", datetime.fromtimestamp(timest))
# nomic = str(timest).split('.')[0]
# print(nomic)
# print("bu da microsec keslms fromtimestamos",
#       datetime.fromtimestamp(int(nomic)))

# ############################ TEST ALANI  SONU ##################################
