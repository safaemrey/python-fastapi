import jwt
from config import SECRET_KEY


def createToken(payload: dict):
    return jwt.encode(payload=payload, key=SECRET_KEY, algorithm='HS512')


def decodeToken(token: str):
    return jwt.decode(jwt=token, key=SECRET_KEY, algorithms='HS512')
