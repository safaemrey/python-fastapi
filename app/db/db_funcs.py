from .session import engine, SessionLocal
from app.db.base_class import Base


from .models.user import *
from .models.auth_log import *


def create():
    """Yazılmış modellerden bir tablo yapısı create eder. Öncelikle Config içerisinde belirtilmiş bir veritabanı
        oluşturulmuş olmalı.
    """
    Base.metadata.create_all(bind=engine)


def get_db():
    """
    get_db func. db kullanacak fonksiyonlar için Depends yazmak amacıyla. her routes içinde import ediliyor.
    generator kullanılıyor.
    """
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()
