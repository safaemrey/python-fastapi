from ..user import *

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow_sqlalchemy.fields import Nested


class UserDetailsMa(SQLAlchemyAutoSchema):
    """
    SqlAlchemy modellerini direkt json olarak almamızı sağlayan bir araç marshmallow. Test ettim. gayet güzel işime
    yarar bir arkdaş. hatta bayıldım.
    """
    class Meta:
        model = UserDetails
        include_relationships = True
        load_instance = True


class UserBasicMaWithDetails(SQLAlchemyAutoSchema):
    """
    SqlAlchemy modellerini direkt json olarak almamızı sağlayan bir araç marshmallow. Test ettim. gayet güzel işime
    yarar bir arkdaş. hatta bayıldım.
    """
    class Meta:
        model = UserBasic
        include_relationships = True
        load_instance = True
    details = Nested(UserDetailsMa, exclude=("userbasic", "id"))


class UserBasicMaWithOUTDetails(SQLAlchemyAutoSchema):
    """
    SqlAlchemy modellerini direkt json olarak almamızı sağlayan bir araç marshmallow. Test ettim. gayet güzel işime
    yarar bir arkdaş. hatta bayıldım.
    """
    class Meta:
        model = UserBasic
        include_relationships = True
        load_instance = True
