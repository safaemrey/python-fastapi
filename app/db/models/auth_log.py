"""
AuthLog
"""
from sqlalchemy import Column, Integer, ForeignKey, DateTime, String
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from app.db.base_class import Base


class AuthLog(Base):
    id = Column(Integer, primary_key=True)
    userId = Column(Integer, ForeignKey('userbasic.id'))
    token = Column(String(length=200))
    tokenCreate = Column(DateTime)
    tokenExpire = Column(DateTime)
