"""
auth içerisindeki modeller. detay için 'ctrl + click' ile takip edip import edilen kütüphanelerin
docstringlerinden model listesini görebilirsiniz.
"""
from .user import UserBasic, UserDetails
from .auth_log import AuthLog
