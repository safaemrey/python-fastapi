"""
UserBasic, UserDetails
"""
from sqlalchemy import Column, Integer, String, SMALLINT, ForeignKey
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash

from app.db.base_class import Base


class UserBasic(Base):
    """ Temel user class. id mail pw ve type içerir. kalan bilgiler gerektiğinde userdetailsden çekilir """
    id = Column(Integer, primary_key=True)
    mail = Column(String(50), nullable=False, unique=True)
    pw_hash = Column(String(128), nullable=False)
    type_ = Column(SMALLINT, nullable=False)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    details = relationship('UserDetails', cascade="all,delete",
                           backref='userbasic', uselist=False)  # one-to-many dden farklı
    # olarak yalnızca uselist=False ekleniyor relationship e o kadar. çohyi.


class UserDetails(Base):
    """ Detay user classı. Giriş yama harici görüntülemelerde extra olarak buradan veri çekilir """
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('userbasic.id'), nullable=False)
    # one-to-one ilişki için gerekli durum bu 2 tabloddaki
    title = Column(String(50), nullable=False)
