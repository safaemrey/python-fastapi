from app.db.crud.base import CRUDBase
from ..models import UserDetails


class CrudUserDetails(CRUDBase[UserDetails]):
    pass


userDetailsCrud = CrudUserDetails(UserDetails)
