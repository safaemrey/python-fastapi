from sqlalchemy.orm import Session
from werkzeug.security import check_password_hash

from app.db.crud.base import CRUDBase
from ..models import UserBasic
from ..schemas import Login
from ..models.ma.user_ma import UserBasicMaWithOUTDetails
from app.exceptions import UserNotFoundError, PasswordNotCorrectError
from .auth_log_crud import authLogCrud


class CrudUserBasic(CRUDBase[UserBasic]):

    def login(self, db: Session = None, login: Login = None):

        user = db.query(self.model).filter(
            self.model.mail == login.mail).first()
        if(user == None):
            raise UserNotFoundError

        if check_password_hash(user.pw_hash, login.passwd):
            userBasicMaWithOUTDetails = UserBasicMaWithOUTDetails(
                exclude=(("pw_hash", "details")))
            user = userBasicMaWithOUTDetails.dump(user)
            token = authLogCrud.createLog(db=db, userId=user.get('id'))
            return {'user': user, 'token': token}

        else:
            raise PasswordNotCorrectError


userBasicCrud = CrudUserBasic(UserBasic)
