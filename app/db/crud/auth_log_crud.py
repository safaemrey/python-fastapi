from sqlalchemy.orm import Session
from datetime import datetime, timedelta

from ..services.token_service import createToken
from app.db.crud.base import CRUDBase
from ..models.auth_log import AuthLog


class CrudAuthLog(CRUDBase[AuthLog]):
    def createLog(self, db: Session = None, userId: int = None):
        """Giriş yapan kullanıcı için log kaydı yapar ve kullanıcı için 10dk geçerli token oluşturup return eder.
        """
        now = datetime.now()
        expire = now + timedelta(minutes=10)
        authLog = AuthLog()
        authLog.tokenCreate = now
        authLog.tokenExpire = expire
        authLog.userId = userId
        token = createToken(payload={
            'userId': userId,
            'expire': expire.timestamp()
        })
        authLog.token = str(token)
        self.create(db=db, obj=authLog)

        return token


authLogCrud = CrudAuthLog(AuthLog)
