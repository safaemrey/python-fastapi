class UserNotFoundError(Exception):
    """ Veritabanında kullacını bulunamadı.

        Parameters:
            code: Hata kodu.
            message: Hata Mesajı.
    """
    code = 201
    message = "Kullanıcı veritabanında bulunamadı!"


class PasswordNotCorrectError(Exception):
    """Şifre yanlış girildi
    """
    code = 202
    message = "Şifre eşleşmiyor!"
