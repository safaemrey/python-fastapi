from fastapi import FastAPI, APIRouter

from app.routes.auth import auth  # auth nesnem aslında bir APIRouter.


# Init app
app = FastAPI()


######################################################################################################################
# AUTH modülünün router kısımlarını app e eklemek için.
app.include_router(auth, prefix="/api/auth")
# Bu satırlar olmazsa benim uygulamamda auth ile alaklı hiçbir şey olmaz. birbirine bağlı modüllerle kontrol ederiz.
# Gerekli olanlar kullanıcı ne ister onu aynı uygulamadan yapabiliriz.
######################################################################################################################
